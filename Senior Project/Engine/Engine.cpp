
//The headers
#include "SDL.h"
#include "SDL_image.h"
#include <string>

//Screen attributes
const int SCREEN_WIDTH = 540;
const int SCREEN_HEIGHT = 420;
const int SCREEN_BPP = 32;

//The surfaces
SDL_Surface *tile[5];
SDL_Surface *background = NULL;
SDL_Surface *screen = NULL;

//The event structure that will be used
SDL_Event event;

SDL_Surface *load_image( std::string filename )
{
    //The image that's loaded
    SDL_Surface* loadedImage = NULL;

    //The optimized image that will be used
    SDL_Surface* optimizedImage = NULL;

    //Load the image
    loadedImage = SDL_LoadBMP( filename.c_str() );

    //If the image loaded
    if( loadedImage != NULL )
    {
        //Create an optimized image
        optimizedImage = SDL_DisplayFormat( loadedImage );

        //Free the old image
        SDL_FreeSurface( loadedImage );
    }

    //Return the optimized image
    return optimizedImage;
}

void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination )
{
    //Temporary rectangle to hold the offsets
    SDL_Rect offset;

    //Get the offsets
    offset.x = x;
    offset.y = y;

    //Blit the surface
    SDL_BlitSurface( source, NULL, destination, &offset );
}

bool init()
{
    //Initialize all SDL subsystems
    if( SDL_Init( SDL_INIT_EVERYTHING ) == -1 )
    {
        return false;
    }

    //Set up the screen
    screen = SDL_SetVideoMode( SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE );

    //If there was an error in setting up the screen
    if( screen == NULL )
    {
        return false;
    }

    //Set the window caption
    SDL_WM_SetCaption( "Blokus", NULL );

    //If everything initialized fine
    return true;
}

void clean_up()
{
	//Free the surfaces
    SDL_FreeSurface( background );
	
	for(int x=0;x<5;x++){
		SDL_FreeSurface( tile[x] );
	}


    //Quit SDL
    SDL_Quit();
}

bool load_files()
{
    //Load the image
    background = load_image( "BlokusBoard.bmp" );
	tile[0] = load_image( "GreyBlokus.bmp" );
    tile[1] = load_image( "GreenBlokus.bmp" );
    tile[2] = load_image( "OrangeBlokus.bmp" );
    tile[3] = load_image( "PurpleBlokus.bmp" );
    tile[4] = load_image( "BlueBlokus.bmp" );

	//If there was an error in loading the image
    if( background == NULL )
    {
        return false;
    }

	for(int x=0;x<5;x++){
		if(tile[x]==NULL){
			return false;
		};
	}



    //If everything loaded fine
    return true;
}

void initBoard(){
	int row,col;
	
	//Create Board
	for(row=0;row<20;row++){
		for(col=0;col<20;col++){
			apply_surface( 20*row+10 , 20*col+10, tile[0], screen );
		}
	}
}

void initBank(){
	int row,col;
	
	//Create Bank
	for(row=0;row<5;row++){
		for(col=0;col<20;col++){
			apply_surface( 420+10+ 20*row , 20*col+10, tile[0], screen );
		}
	}
}

void updateBoard(){

}

void updateBank(){

}

int main( int argc, char* args[] )
{
    //Make sure the program waits for a quit
    bool quit = false;

    //Initialize
    if( init() == false )
    {
        return 3;
    }

    //Load the files
    if( load_files() == false )
    {
        return 2;
    }

    //Apply the surface to the screen
    apply_surface( 0, 0, background, screen );
	initBoard();
	initBank();

    //Update the screen
    if( SDL_Flip( screen ) == -1 )
    {
        return 1;
    }

    //While the user hasn't quit
    while( quit == false )
    {
        //While there's an event to handle
        while( SDL_PollEvent( &event ) )
        {
            //If the user has Xed out the window
            if( event.type == SDL_QUIT )
            {
                //Quit the program
                quit = true;
            }
        }
    }

    //Free the surface and quit SDL
    clean_up();

    return 0;
}
