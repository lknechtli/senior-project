#include "stdafx.h"
#include "SwipeLeftSegments.h"

        GesturePartResult SwipeLeftSegment1::CheckGesture(const NUI_SKELETON_DATA & skeleton)
        {

            // right hand in front of right shoulder
			if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].z < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_RIGHT].z && skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_LEFT].y < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_CENTER].y)
            {
                // right hand below shoulder height but above hip height
                if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].y < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HEAD].y && skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].y > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HIP_CENTER].y)
                {
                    // right hand right of right shoulder
                    if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].x > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_RIGHT].x)
                    {
                        return Succeed;
                    }
                    return Pausing;
                }
                return Fail;
            }
            return Fail;
        }



		GesturePartResult SwipeLeftSegment2::CheckGesture(const NUI_SKELETON_DATA & skeleton)
        {
            // right hand in front of right shoulder
            if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].z < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_RIGHT].z && skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_LEFT].y < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_CENTER].y)
            {
                // right hand below shoulder height but above hip height
                if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].y < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HEAD].y && skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].y > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HIP_CENTER].y)
                {
                    // right hand left of right shoulder & right of left shoulder
                    if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].x < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_RIGHT].x && skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].x > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_LEFT].x)
                    {
                        return Succeed;
                    }
                    return Pausing;
                }
                return Fail;
            }
            return Fail;
        }



		GesturePartResult SwipeLeftSegment3::CheckGesture(const NUI_SKELETON_DATA & skeleton)
        {
            // //Right hand in front of right Shoulder
            if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].z < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_RIGHT].z && skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_LEFT].y < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_CENTER].y)
            {
                // //right hand below shoulder height but above hip height
                if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].y < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_CENTER].y && skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].y > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HIP_CENTER].y)
                {
                    // //right hand left of center hip
                    if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].x < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_LEFT].x)
                    {
                        return Succeed;
                    }

                    return Pausing;
                }

                return Fail;
            }

            return Fail;
        }
