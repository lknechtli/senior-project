#include "stdafx.h"
#include "Gesture.h"

Gesture::Gesture(GestureType name, IRelativeGestureSegment * parts[], int l)
    {
		currentGesturePart = 0;
		pausedFrameCount = 10;
		frameCount = 0;
		paused = false;
		GestureRecognized = false;
		type = name;
		length = l;
		for(int x=0;x<length;x++){
			gestureParts[x] = parts[x];
		}

        
    }

    /// <summary>
    /// Updates the gesture.
    /// </summary>
    /// <param name="data">The skeleton data.</param>
    void Gesture::UpdateGesture(NUI_SKELETON_DATA data)
    {
        if (paused)
        {
            if (this->frameCount == this->pausedFrameCount)
            {
                this->paused = false;
            }

            this->frameCount++;
        }
		GesturePartResult result = gestureParts[currentGesturePart]->CheckGesture(data);
        if (result == Succeed)
        {
            if (this->currentGesturePart + 1 < length)//gestureParts->Length)
            {
                this->currentGesturePart++;
                this->frameCount = 0;
                this->pausedFrameCount = 10;
                this->paused = true;
            }
            else
            {
				this->GestureRecognized=TRUE;
					//gesture is recognized

                    //this->GestureRecognized(this, new GestureEventArgs(this->name, data.TrackingId));
                    this->Reset();

            }
        }
        else if (result == Fail || this->frameCount == 50)
        {
            this->currentGesturePart = 0;
            this->frameCount = 0;
            this->pausedFrameCount = 5;
            this->paused = true;
        }
        else
        {
            this->frameCount++;
            this->pausedFrameCount = 5;
            this->paused = true;
        }
    }

    /// <summary>
    /// Resets this instance.
    /// </summary>
    void Gesture::Reset()
    {
        this->currentGesturePart = 0;
        this->frameCount = 0;
        this->pausedFrameCount = 5;
        this->paused = true;
    }













