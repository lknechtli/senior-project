#include "stdafx.h"
#include "SwipeDownSegments.h"

        GesturePartResult SwipeDownSegment1::CheckGesture(const NUI_SKELETON_DATA & skeleton)
        {
            // right hand in front of right shoulder
            if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].z < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_RIGHT].z && skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].y < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_CENTER].y)
            {
                // right hand below head height and hand higher than elbow
                if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].y < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HEAD].y && skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].y > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_RIGHT].y)
                {
                    // right hand right of right shoulder
                    if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].x > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_RIGHT].x)
                    {
                        return Succeed;
                    }
                    return Pausing;
                }
                return Fail;
            }
            return Fail;
        }
         



		GesturePartResult SwipeDownSegment2::CheckGesture(const NUI_SKELETON_DATA & skeleton)
        {
            // right hand in front of right shoulder
            if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].z < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_RIGHT].z && skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].y < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_CENTER].y)
            {
                // right hand below right elbow
                if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].y < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_RIGHT].y)
                {
                    // right hand right of right shoulder
                    if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].x > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HIP_RIGHT].x)
                    {
                        return Succeed;
                    }
                    return Pausing;
                }
                return Fail;
            }
            return Fail;
        }



		GesturePartResult SwipeDownSegment3::CheckGesture(const NUI_SKELETON_DATA & skeleton)
        {
            // //Right hand in front of right Shoulder
            if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].z < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_RIGHT].z && skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].y < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_CENTER].y)
            {
                // right hand below hip
                if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].y < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HIP_RIGHT].y)
                {
                    // right hand right of right shoulder
                    if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].x > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HIP_RIGHT].x)
                    {
                        return Succeed;
                    }
                    return Pausing;
                }

                // Debug.WriteLine("GesturePart 2 - right hand below shoulder height but above hip height - FAIL");
                return Fail;
            }

            // Debug.WriteLine("GesturePart 2 - Right hand in front of right Shoulder - FAIL");
            return Fail;
        }
