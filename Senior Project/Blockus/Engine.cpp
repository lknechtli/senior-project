#include "stdafx.h"
#include <vector>
#include <cstdlib>
#include "Engine.h"
#include "Player.h"
#include "Piece.h"

#include <Windows.h>
using namespace std;
Engine::Engine(void)
{
	background = NULL;
	screen = NULL;
	select=NULL;
	for(int i = 0; i < 5; i++){
		tile[i]= NULL;
	}
	if(!loadFiles()){
		cleanUp();
		std::cerr<<"Error loading image files"<<std::endl;
		system("pause");
		exit(-1);
	}
	
}


Engine::~Engine(void)
{
	cleanUp();
}

bool Engine::loadFiles()
{
    //Load the image
	/*
	For transparent backgrounds on images (like for text or something)
	SDL_Surface picture = IMG_Load("picture.png");
	SDL_SetColorKey(picture, SDL_SRCCOLORKEY, SDL_MapRGB(picture->format, 0, 0, 255)); //set blue as transparency color
	
	*/
    background = SDL_LoadBMP( "data/BlokusBoard_new.bmp" );
	select = SDL_LoadBMP("data/Select.bmp");
	tile[0] = SDL_LoadBMP( "data/GreyBlokus.bmp" );
    tile[1] = SDL_LoadBMP( "data/GreenBlokus.bmp" );
    tile[2] = SDL_LoadBMP( "data/OrangeBlokus.bmp" );
    tile[3] = SDL_LoadBMP( "data/PurpleBlokus.bmp" );
    tile[4] = SDL_LoadBMP( "data/BlueBlokus.bmp" );
	playerSurfaces[0]=SDL_LoadBMP( "data/playerone.bmp");
	playerSurfaces[1]=SDL_LoadBMP("data/playertwo.bmp");
	scoreSurface=SDL_LoadBMP("data/score.bmp");
	numbers = SDL_LoadBMP("data/numbers.bmp");
	SDL_Rect numberOffset;
	numberOffset.x=0;
	numberOffset.y=0;
	numberOffset.h=30;
	numberOffset.w=20;
	SDL_Rect digitOffset;
	digitOffset.x=0;
	digitOffset.y=0;

//	SDL_Surface * numberSurfs[10];
	for(int i = 0; i < 10; i++){
		numberSurfs[i]=SDL_CreateRGBSurface(0,20,30,32,0,0,0,0);
	}
	for(int i = 0; i < 10; i++){
		SDL_BlitSurface(numbers,&numberOffset,numberSurfs[i],&digitOffset);
		numberOffset.x+=20;
	}
	//If there was an error in loading the image
    if( background == NULL )
    {
        return false;
    }

	for(int x=0;x<5;x++){
		if(tile[x]==NULL){
			return false;
		};
	}
    //If everything loaded fine
    return true;
}

void Engine::cleanUp(void)
{
	//Free the surfaces
    SDL_FreeSurface( background );
	
	for(int x=0;x<5;x++){
		SDL_FreeSurface( tile[x] );
	}
	

    //Quit SDL
    SDL_Quit();
}
void Engine::applySurface( int x, int y, SDL_Surface* source, SDL_Surface* destination )
{
    //Temporary rectangle to hold the offsets
    SDL_Rect offset;
    //Get the offsets
    offset.x = x;
    offset.y = y;
    //Blit the surface
    SDL_BlitSurface( source, NULL, destination, &offset );
}
void Engine::initBoard(){
	int row,col;
	
	//Create Board
	for(row=0;row<20;row++){
		for(col=0;col<20;col++){
			applySurface( 20*row+10, 20*col+10, tile[0], screen );
		}
	}
}
/*
	board space: 580 x 460
	Piece grid: 10x10 -> 428x428
	each piece: 20x20
	gap between pieces: 2
*/
void Engine::displayBoard(int ** board){
	applySurface(0,0,background,screen);
	for(int row=0;row<20;row++){
		for(int col=0;col<20;col++){
			applySurface(22*row+10, 22*col+10, tile[board[row][col]], screen); //uses color of tile given the right board array
			
		}
	}
}
void Engine::initBank(){
	int row,col;
	
	//Create Bank
	for(row=0;row<5;row++){
		for(col=0;col<20;col++){
			applySurface(420+10+20*row, 20*col+10, tile[0], screen);
		}
	}
}
void Engine::displayBank(Player * activePlayer){
	displaySelectedBankPiece(activePlayer);
	vector<Piece*>::iterator pieceIter = activePlayer->getBank();
	for(int i = 0; i < 4 && pieceIter != activePlayer->getBankEnd(); i++){
		displayPiece(*pieceIter,i);
		pieceIter++;
		//incriment index everytime.
		//display the current piece
		//currentPiece = pieceIter;
		//if(currentPiece != NULL){
		//	//display current piece
		//	displayPiece(currentPiece, i);
		//}
	}
	
}
void Engine::displayPiece(Piece * piece, int bankPosition){
	int ** shape = piece->getShape();
	for(int col = 0; col < 5; col++){
		for(int row = 0; row < 5; row++){
			if(shape[col][row]==1)
				applySurface(465 + 22*col, 10+22*row+22*5*bankPosition,tile[piece->getColor()],screen);
		}
	}
}
void Engine::displaySelectedBankPiece(Player * activePlayer){
	applySurface(465, activePlayer->getBankIndex()*22*5+10,select,screen);
}
void Engine::displayActivePiece(Piece * active, int x, int y){ //TODO: implement
	for(int col = 0; col < 5; col++){
		for(int row = 0; row < 5; row++){
			if(active->getShape()[col][row] == 1){
				applySurface(x + 22*col,y+22*row, tile[active->getColor()],screen);
			}
		}
	}
}
void Engine::displayPlayer(Player * activePlayer, int totalScore){
	int pnumber = activePlayer->getPlayerNumber();
	if(pnumber == 0 || pnumber == 2)
	{
		//player one
		applySurface(0,infoCoords,playerSurfaces[0],screen);
		
	}
	else
	{
		//player two
		applySurface(0,infoCoords,playerSurfaces[1],screen);
	}
	displayScore(totalScore);
}
void Engine::displayScore(int score){
	applySurface(23*10,465,scoreSurface,screen);
	//first digit
	
	
	int onesdigit=0,tensdigit=0, hundredsdigit=0;;
	if(score!=0){
		onesdigit = score%10; 
		tensdigit = score/10; 
		hundredsdigit = score/100;
	}
	if(onesdigit==0)
		onesdigit=10;
	if(tensdigit==0){
		tensdigit=10;
	}
	if(hundredsdigit==0){
		hundredsdigit=10;
	}


	applySurface(23*10+140,465,numberSurfs[hundredsdigit-1],screen);
	applySurface(23*10+140+20,465,numberSurfs[tensdigit-1],screen);
	applySurface(23*10+140+40,465,numberSurfs[onesdigit-1],screen);
	
	//applySurface(23*10+140,465,numbers,screen);

}
SDL_Surface* Engine::getSurface(){
	return screen;
}
void Engine::setSurface(SDL_Surface* surface){
	screen = surface;
}