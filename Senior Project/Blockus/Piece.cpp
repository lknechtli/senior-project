#include "stdafx.h"
#include "Piece.h"


void rotateMatrixCW(int **& shape);
void rotateMatrixCCW(int **& shape);
void initShape(int **& shape, int pieceNumber, int ** pieceArray);
/*
	pieceArray is of format:
	each piece is 1 row (25 slots) , 21 rows in all
*/
Piece::Piece(int pieceNumber, int ** pieceArray, int pieceColor, int * piecePosition)
{
	initShape(shape, pieceNumber, pieceArray);
	position = new int[2];
	for(int i = 0; i < 2; i++){
		position[i]=piecePosition[i];
	}
	color = pieceColor;
	state = 0;
	this->pieceNumber = pieceNumber;
}

void initShape(int **& shape, int pieceNumber, int ** pieceArray){
	int temp[25];
	shape = new int*[5];
	for(int i=0; i<5; i++){
		shape[i]=new int[5];
	}

	for(int i = 0; i<25; i++){
		temp[i]=pieceArray[pieceNumber][i];
	}
	for(int i = 0; i<5; i++){
		for(int j = 0; j < 5; j++){
			shape[i][j]=temp[i*5+j];
		}
	}
}
Piece::~Piece(void)
{
	for(int i = 0; i < 5; i++){
		delete [] shape[i];
	}
	delete [] shape;
	delete [] position;
}
void Piece::move(int pos[3])
{
	for(int i = 0; i<0; i++){
		position[i] = pos[i];
	}
}
void Piece::display(void)
{
	//display piece 
}
void Piece::rotate(int dir)
{
	//if dir = 1 rotate cw, -1 ccw, 0 do nothing
	switch(dir){
		case 1: rotateMatrixCW(shape); break;
		case -1: rotateMatrixCCW(shape); break;
		case 0: break;
		default:
			std::cerr<<"Invalid rotate direction for rotate. This should not happen! Please check your code."<<std::endl;
	}
}
void rotateMatrixCW(int **& shape){
/*
	[i,j] -> [j,4-i]
	  j
	i 1  2  3  4  5     21 16 11 6  1
	  6  7  8  9  10    22 17 12 7  2
	  11 12 13 14 15 -> 23 18 13 8  3
	  16 17 18 19 20    24 19 14 9  4
	  21 22 23 24 25    25 20 15 10 5
*/

	int ** temp = new int*[5];
	for(int i = 0; i < 5; i++){
		temp[i] = new int[5];
	}
	for(int i = 0; i<5;i++){
		for(int j = 0; j<5; j++){
			temp[j][4-i]=shape[i][j];
		}
	}
	for(int i = 0; i<5; i++){
		delete [] shape[i];
	}
	delete [] shape;
	shape = temp;
}
void rotateMatrixCCW(int **& shape){
	/*
	[i,j] -> [4-j,i]
	  j
	i 1  2  3  4  5     5  10 15 20 25
	  6  7  8  9  10    4  9  14 19 24
	  11 12 13 14 15 -> 3  8  13 18 23
	  16 17 18 19 20    2  7  12 17 22
	  21 22 23 24 25    1  6  11 16 21
*/

	int ** temp = new int*[5];
	for(int i = 0; i < 5; i++){
		temp[i] = new int[5];
	}
	for(int i = 0; i<5;i++){
		for(int j = 0; j<5; j++){
			temp[4-j][i]=shape[i][j];
		}
	}
	for(int i = 0; i<5; i++){
		delete [] shape[i];
	}
	delete [] shape;
	shape = temp;
}

bool Piece::setPosition(int x, int y){ // todo: use correct numbers for limits. these are here for testing bcs im too lazy to calculate them now
	int xpos = position[0]+x;
	int ypos = position[1]+y;
	position[0]=x;
	position[1]=y;
	return true;
	
	
}