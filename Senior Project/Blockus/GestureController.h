#pragma once

#include "Gesture.h"
#include "SwipeLeftSegments.h"
#include "JoinedHandsSegment.h"
#include "SwipeRightSegments.h"
#include "WaveLeftSegments.h"
#include "SwipeUpSegments.h"
#include "SwipeDownSegments.h"
#include "LeftClickSegments.h"

//#include "Vector3d.h"
//#include "Scene.h"

#define numGestures 6

class GestureController  
{
private:
	/// <summary>
    /// The list of all gestures we are currently looking for
    /// </summary>
	Gesture * gestures[numGestures];

public: 

	/// <summary>
    /// Initializes a new instance of the <see cref="GestureController"/> class.
    /// </summary>
	GestureController();


	/// <summary>
    /// Updates all gestures.
    /// </summary>
    /// <param name="data">The skeleton data.</param>
	void UpdateAllGestures(NUI_SKELETON_DATA data);



};


