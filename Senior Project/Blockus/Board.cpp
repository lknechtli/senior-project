#include "stdafx.h"
#include <fstream>
#include <string>
#include "Board.h"
#include "Player.h"
#include <Windows.h>
#include <vector>
//data file location
static char* const shapefile="data/Pieces.txt";

Board::Board(){
	keyModifier = false;
	board = new int*[20];
	for(int i = 0; i < 20; i++){
		board[i]= new int[20];
		for(int ii = 0; ii < 20; ii++){
			board[i][ii]=0;
		}
	}
	init();
}
Board::~Board(){
	for(int i = 0; i<21;i++){
		delete[] pieceArray;
	}
	delete[] pieceArray;
	for(int i = 0; i<20;i++){
		delete[] board;
	}
	delete[] board;
}
void Board::init(){
	// read in piece definitions
	std::cout<<"Reading Piece definitions"<<std::endl;
	pieceArray = readPieces(shapefile);
	std::cout<<"Pieces successfully read"<<std::endl;
	// initialize players (who initialize pieces)
	for(int i = 0; i < numPlayers; i++){
		players[i].init(pieceArray,i+1,i);
	}
	for(int i = 0; i < numPlayers; i++){
		playerScores[i]=0;
	}
	players[0].active = true;
	gameMode=1; //piece choosing gamemode
}
void Board::resetBoard(void){
	for(int i = 0; i<20;i++){
		delete[] board;
	}
	delete[] board;
	board = new int*[20];
	for(int i = 0; i < 20; i++){
		board[i]= new int[20];
		for(int ii = 0; i < 20; i++){
			board[i][ii]=0;
		}
	}
	//TODO: check this works
	delete[] players;
	for(int i = 0; i < numPlayers; i++){
		players[i].init(pieceArray,i+1,i);
	}
	for(int i = 0; i < numPlayers; i++){
		playerScores[i]=0;
	}
	players[0].active = true;
	gameMode=1; //piece choosing gamemode
}
void Board::passInput(signed int key){
	//if game state in piece choosing, call bank manager method
	//if game state in moving piece, call piece movement method

	switch(gameMode){
	case bankMode:
		switch(key){
		case SDLK_SPACE:
			//switch to piece placing mode
			gameMode = pieceMode;
			break;
		default:
			getActivePlayer()->passBankInput(key);
		}
		break;
	case pieceMode:
		switch(key){
		case SDLK_ESCAPE: //go back to bank mode
			gameMode = bankMode;
			break;
		case SDLK_UP:
		case SDLK_DOWN:
		case SDLK_LEFT:
		case SDLK_RIGHT:
			movePiece(key);
			break;
		case SDLK_SPACE: //place piece
			placePiece();
			break;
		case SDLK_s:// skip turn (if can't place piece)
			getActivePlayer()->endGame = true;
			if(getActivePlayer()->lastPlacedPiece == 0)
				playerScores[getActivePlayer()->getPlayerNumber()]+=20;
			else
				playerScores[getActivePlayer()->getPlayerNumber()]+=15;
			nextPlayer();
			break;
		case SDLK_z:
			getActivePlayer()->getActivePiece()->rotate(-1);
			break;
		case SDLK_x:
			getActivePlayer()->getActivePiece()->rotate(1);
			break;
		}
	}
}
void Board::movePiece(unsigned int key){
	Piece * toMove = getActivePlayer()->getActivePiece();
	int * location = toMove->getPosition();
	switch(key){
	case SDLK_UP:
		if(keyModifier){
			location[1]-=20;
		}
		location[1]-=22;
		break;
	case SDLK_DOWN:
		if(keyModifier){
			location[1]+=20;
		}
		location[1]+=22;
		break;
	case SDLK_LEFT:
		if(keyModifier){
			location[0]-=20;
		}
		location[0]-=22;
		break;
	case SDLK_RIGHT:
		if(keyModifier){
			location[0]+=20;
		}
		location[0]+=22;
		break;
	}
	toMove->setPosition(location[0],location[1]);
}
void Board::movePiece(int x, int y){
	Piece * toMove = getActivePlayer()->getActivePiece();
	int * location = toMove->getPosition();
	location[0]=22*x+10;
	location[1]=22*y+10;
	//cout<<"x:"<<x<<" y:"<<y<<" locx:"<<location[0]<<" locy:"<<location[1]<<endl;
	
}
bool Board::placePiece(){
	/*
	order of instructions:
	1) snap piece
	2) change board colors
	3) delete piece
	4) change game mode
	5) change to next player
	*/

	Piece * piece = getActivePlayer()->getActivePiece();
	if(validPosition(piece)){
		int scoreAdd = setBoardTiles(piece);
		getActivePlayer()->firstBlock=false;
		getActivePlayer()->removeActivePiece();
		playerScores[getActivePlayer()->getPlayerNumber()]+=scoreAdd;
		cout<<"Player "<<getActivePlayer()->getPlayerNumber()<<" score: "<<playerScores[getActivePlayer()->getPlayerNumber()]<<endl;
		gameMode = bankMode;
		nextPlayer();
		return true;
	}
	else
		return false;
}
bool Board::validPosition(Piece * piece){
	/*
	 1) check that all blocks below piece are open
	 2) check that at least one block is diagonal to one of the same color, or it's the first piece played on the corner
	 3) check that no piece is adjacent to one of the same color
	*/

	//1)
	/*10,10 are coordinates for upper left hand corner of board
	32,32 for next one diagonally down to the right
	then 54,54 (add 22 for each block down and to the right)
	to translate piece position into board grid coordinates:*/
	int xcoords = (piece->getPosition()[0]-10)/22;
	int ycoords = (piece->getPosition()[1]-10)/22;
	int ** pieceShape = piece->getShape();
	for(int col = 0; col < 5; col++){
		for(int row = 0; row < 5; row ++){
			if(pieceShape[col][row]==1){
				cout<<xcoords+col<<","<<ycoords+row<<endl;
				if((xcoords+col < 0 || xcoords+col>19 || ycoords+row < 0 ) || ycoords+row>19 || (board[xcoords+col][ycoords+row] != 0)){
					//if block not on board or block over another color block
					cout<<"block collision, or not on board"<<endl;
					return false;
				}
			}
		}
	}

	//2) check that a diagonal is the same color
	//TODO: works only for upper left
	bool diag = false;
	for(int col = 0; col < 5 && !diag; col++){
		for(int row = 0; row < 5 && !diag; row++){
			//check if it's in a corner and is first piece placed
			//upper left corner
			if(pieceShape[col][row]!=1){
				continue;
			}
			//TODO: make this work
			if(xcoords+col == 0 && ycoords+row == 0){
				if(getActivePlayer()->firstBlock)
					diag = true;
			}
			//check upper right corner
			else if(xcoords+col==19 && ycoords+row == 0)
			{
				if(getActivePlayer()->firstBlock)
					diag = true;
			}
			//check lower left corner
			else if(xcoords+col==0 && ycoords+row == 19)
			{
				if(getActivePlayer()->firstBlock)
					diag = true;
			}
			//check lower right corner
			else if(xcoords+col==19 && ycoords+row==19)
			{
				if(getActivePlayer()->firstBlock)
					diag = true;
			}
			//TODO: check if block being checked is an edge, then check diagonals for same color block
			//left edge
			else if(xcoords+col==0){
				if(board[xcoords+col+1][ycoords+row-1] == piece->getColor()){
					diag = true;
				}
				else if(board[xcoords+col+1][ycoords+row+1] == piece->getColor()){
					diag = true;
				}
			}
			//right edge
			else if(xcoords+col == 19){
				if(board[xcoords+col-1][ycoords+row-1] == piece->getColor()){
					diag = true;
				}
				else if(board[xcoords+col-1][ycoords+row+1] == piece->getColor()){
					diag = true;
				}
			}
			//top edge
			else if(ycoords+row==0){ 
				if(board[xcoords+col-1][ycoords+row+1] == piece->getColor()){
					diag = true;
				}
				else if(board[xcoords+col+1][ycoords+row+1] == piece->getColor()){
					diag = true;
				}
			}
			//bottom edge
			else if(ycoords+col==19){
				if(board[xcoords+col-1][ycoords+row-1] == piece->getColor()){
					diag = true;
				}
				else if(board[xcoords+col+1][ycoords+row-1] == piece->getColor()){
					diag = true;
				}
			}
			//check for blocks in the middle (indexes 1-18)
			else if(xcoords+col > 0 && ycoords+col>0){
				//check upper left
				if(board[xcoords+col-1][ycoords+row-1] == piece->getColor()){
					diag = true;
				}
				//check upper right
				if(board[xcoords+col+1][ycoords+row-1] == piece->getColor()){
					diag = true;
				}
				//lower left
				if(board[xcoords+col-1][ycoords+row+1] == piece->getColor()){
					diag = true;
				}
				//lower right
				if(board[xcoords+col+1][ycoords+row+1] == piece->getColor()){
					diag = true;
				}
			}
		}
	}
	
	//3) check surrounding top, bottom, left, right for tiles of same color
	//TODO: make sure it doesn't check beyond the bounds of the board.
	for(int col = 0; col < 5; col++){
		for(int row = 0; row < 5; row++){
			if(pieceShape[col][row]!=1){
				continue;
			}
			//top
			if(ycoords+row-1>=0){
				if(board[xcoords+col][ycoords+row-1] == piece->getColor()){
					return false;
				}
			}
			//left
			if(xcoords+col-1>=0){
				if(board[xcoords+col-1][ycoords+row] == piece->getColor()){
					return false;
				}
			}
			//right
			if(xcoords+col+1<20){
				if(board[xcoords+col+1][ycoords+row] == piece->getColor()){
					return false;
				}
			}
			//bottom
			if(ycoords+row+1<20){
				if(board[xcoords+col][ycoords+row+1] == piece->getColor()){
					return false;
				}
			}
		}
	}

	return diag&true;
}

int Board::setBoardTiles(Piece * piece){
	int ** tiles = piece->getShape();
	int numTilesSet=0;
	int xcoords = (piece->getPosition()[0]-10)/22;
	int ycoords = (piece->getPosition()[1]-10)/22;
	for(int row = 0; row < 5; row++){
		for(int col = 0; col < 5; col++){
			if(tiles[col][row]==1){
				board[xcoords+col][ycoords+row]=piece->getColor();
				++numTilesSet;
			}
		}
	}
	return numTilesSet;
}
void Board::nextPlayer(){
	int active = 0;
	for(; active < numPlayers; active++){
		if(players[active].active)
			break;
	}
	players[active].active = false;
	/*if(active+1>=numPlayers){
		players[0].active = true;
	}
	else{
		players[active + 1].active=true;
	}*/
	for(int i = 1; i < 5; i++){
		active++;
		if(active>3){
			active-=4;
		}
		if(!players[active].endGame){
			players[active].active = true;
			return;
		}
	}
	
	gameMode = defaultMode; //end game
	int player1score = playerScores[0]+playerScores[2];
	int player2score = playerScores[1]+playerScores[3];
	
	cout<<"####################################################"<<endl;
	cout<<"#                                                  #"<<endl;
	cout<<"#                   Game Over                      #"<<endl;
	cout<<"#                                                  #"<<endl;
	cout<<"####################################################"<<endl;
	if(player1score > player2score){
		cout<<endl<<"Player 1 wins with score: "<<player1score<<endl;
	}
	else if(player1score < player2score){
		cout<<endl<<"Player 2 wins with score: "<<player2score<<endl;
	}
	else{
		cout<<endl<<"Game ends in tie with score: "<<player1score<<endl;
	}

}
/*
	board space: 580 x 460
	Piece grid: 10x10 -> 428x428
	each piece: 20x20
	gap between pieces: 2
	just snap to top left hand corner of block when placing ( including gap to right and below)
	can think of placing blocks as 12x12, offset by 10 x, 10 y

*/
int * Board::getClosestPos(int * const pos){
	int * closestPos = new int[2];
	closestPos[0] = pos[0];
	closestPos[1] = pos[1];
	closestPos[0]-=10;
	closestPos[1]-=10;
	//divide by 12 to get block place (its an int, so any fractional difference lost is the snapping), then multiply by 12, add 10 to get actual coordinate
	if((closestPos[0] > 0) && (closestPos[1] > 0)){
		closestPos[0]/=12; closestPos[1]/=12;
		closestPos[0]*=12; closestPos[1]*=12;
		closestPos[0]+=10; closestPos[0]+=10;
	}
	else{
		closestPos[0]=0; closestPos[1]=0;
	}

	return closestPos;
}
int ** Board::readPieces(char * pieceFile){
	//check that pieces read from file create array of right size
	int ** shapes = new int*[21];
	for(int i = 0; i < 21; i++){
		shapes[i]=new int[25];
	}
	std::string line;
	std::ifstream myfile;
	myfile.open(pieceFile);
	if(!myfile.good()){
		std::cerr<<"Error reading file."<<std::endl;
		system("pause");
		exit(-1);
	}
	int pieceCount = 0;
	while(std::getline(myfile,line))
	{
		pieceCount ++;
		if(pieceCount >21 || line.length()!=25){
			std::cerr<<"Invalid piece file format: error 1 - pieceCount ="<<pieceCount<<" line.length = "<<line.length()<<std::endl;
			myfile.close();
			system("pause");
			exit(-1);
		}
		for(int i = 0; i<25; i++){
			shapes[pieceCount-1][i]=(line[i]-48);
			switch(shapes[pieceCount-1][i]){
			case 0: break;
			case 1: break;
			default: 
				std::cerr<<"Invalid piece file format: error 2"<<std::endl;
				myfile.close();
				system("pause");
				exit(-1);
			}
		}
	}
	myfile.close();
	return shapes;
}
int ** Board::getBoard(void){
	return board;
}
Player * Board::getActivePlayer(void){
	for(int i = 0; i < numPlayers; i++){
		if(players[i].active)
			return &players[i];
	}
	return NULL;
}
bool Board::isPlacingPiece(void){
	return gameMode == 3;
}
int Board::getActivePieceX(void){
	return getActivePlayer()->getActivePiece()->getPosition()[0];
}
int Board::getActivePieceY(void){
	return getActivePlayer()->getActivePiece()->getPosition()[1];
}
void Board::setMode(int mode){
	switch(mode){
	case 0:
	case 1:
	case 2:
	case 3:
		gameMode = mode;
		break;
	default:
		std::cerr<<"Invalid game mode set: "<<mode<<std::endl;
	}
}
int Board::getPlayerScore(void){
	//TODO: fix score reporting
	int player1 = 0, player2 = 0;
	player1 = playerScores[0]+playerScores[2];
	player2 = playerScores[1]+playerScores[3];
	if(players[0].active || players[2].active){
		return player1;
	}
	else{
		return player2;
	}
}