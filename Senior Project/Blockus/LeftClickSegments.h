#pragma once
 
#include "stdafx.h"
#include "IRelativeGestureSegment.h"
#include "GestureEnumTypes.h"
#include "Vector3D.h"

#define LeftClickLength 3
#define PI 3.1415926

    class LeftClickSegment1 : public IRelativeGestureSegment
    {
        /// <summary>
        /// Checks the gesture.
        /// </summary>
        /// <param name="skeleton">The skeleton.</param>
        /// <returns>GesturePartResult based on if the gesture part has been completed</returns>
		public:
			GesturePartResult CheckGesture(const NUI_SKELETON_DATA & skeleton);
    };


    class LeftClickSegment2 : public IRelativeGestureSegment
    {
        /// <summary>
        /// Checks the gesture.
        /// </summary>
        /// <param name="skeleton">The skeleton.</param>
        /// <returns>GesturePartResult based on if the gesture part has been completed</returns>
		public:
			GesturePartResult CheckGesture(const NUI_SKELETON_DATA & skeleton);
    };

	class LeftClickSegment3 : public IRelativeGestureSegment
    {
        /// <summary>
        /// Checks the gesture.
        /// </summary>
        /// <param name="skeleton">The skeleton.</param>
        /// <returns>GesturePartResult based on if the gesture part has been completed</returns>
		public:
			GesturePartResult CheckGesture(const NUI_SKELETON_DATA & skeleton);
    };