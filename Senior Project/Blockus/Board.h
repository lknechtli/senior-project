#pragma once
#include "Player.h"
/**********************
	Board class:
		Board representation: 2d array of objects which contain whatever information is required to render that area of the board.
		20x20 array of int numbers representing colors
		colors :
		0 grey
		1 green
		2 orange
		3 purple
		4 blue
*/


class Board{
private:
	enum{defaultMode = 1, bankMode=2, pieceMode=3, numPlayers=4};
	
	int ** pieceArray;
	int ** readPieces(char * pieceFile);
	int ** board;
	Player players[numPlayers];
	int playerScores[numPlayers]; //player scores
	int gameMode; 
	bool placePiece(void);
	int * getClosestPos(int * pos);
	void movePiece(unsigned int key);
	bool validPosition(Piece * piece);
	int setBoardTiles(Piece *);
	void nextPlayer(void);
	
public:
	Board();
	~Board();
	void display();
	void init();
	void passInput(signed int key);
	void resetBoard(void);
	int ** getBoard(void);
	Player * getActivePlayer(void);
	bool isPlacingPiece(void);
	int getActivePieceX(void);
	int getActivePieceY(void);
	void setMode(int mode);
	int getPlayerScore(void);
	bool keyModifier;
	void movePiece(int x, int y);
	bool gameOver(){return gameMode == defaultMode;}
};
