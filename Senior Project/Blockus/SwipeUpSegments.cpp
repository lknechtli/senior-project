#include "stdafx.h"
#include "SwipeUpSegments.h"

        GesturePartResult SwipeUpSegment1::CheckGesture(const NUI_SKELETON_DATA & skeleton)
        {
            //// right hand in front of right elbow
            //if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].z < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_RIGHT].z)
            //{
            //    // right hand below shoulder height but above hip height
            //    if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].y < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HIP_RIGHT].y && skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].y > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HIP_CENTER].y)
            //    {
            //        // right hand right of right shoulder
            //        if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].x > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_RIGHT].x)
            //        {
            //            return Succeed;
            //        }
            //        return Pausing;
            //    }
            //    return Fail;
            //}
            //return Fail;

			            // //Right hand in front of right shoulder
            if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].z < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_RIGHT].z)
            {
                // right hand above head
                if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].y > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HEAD].y)
                {
                    // right hand right of right shoulder
                    if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].x > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_RIGHT].x)
                    {
                        return Succeed;
                    }
                    return Pausing;
                }

                // Debug.WriteLine("GesturePart 2 - right hand below shoulder height but above hip height - FAIL");
                return Fail;
            }

            // Debug.WriteLine("GesturePart 2 - Right hand in front of right Shoulder - FAIL");
            return Fail;
        }
         



		GesturePartResult SwipeUpSegment2::CheckGesture(const NUI_SKELETON_DATA & skeleton)
        {
            // right hand in front of right shoulder
            if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].z < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_RIGHT].z)
            {
                // right hand above right shoulder
                if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].y > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_RIGHT].y)
                {
                    // right hand right of right shoulder
                    if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].x > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_RIGHT].x)
                    {
                        return Succeed;
                    }
                    return Pausing;
                }
                return Fail;
            }
            return Fail;
        }



		GesturePartResult SwipeUpSegment3::CheckGesture(const NUI_SKELETON_DATA & skeleton)
        {
            // //Right hand in front of right shoulder
            if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].z < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_RIGHT].z)
            {
                // right hand above head
                if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].y > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HEAD].y)
                {
                    // right hand right of right shoulder
                    if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].x > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_RIGHT].x)
                    {
                        return Succeed;
                    }
                    return Pausing;
                }

                // Debug.WriteLine("GesturePart 2 - right hand below shoulder height but above hip height - FAIL");
                return Fail;
            }

            // Debug.WriteLine("GesturePart 2 - Right hand in front of right Shoulder - FAIL");
            return Fail;
        }
