#include "stdafx.h"
#include "Player.h"
#include "Piece.h"
#include "SDL/SDL.h"
#include "SDL/SDL_opengl.h"
using namespace std;
static const int numPieces = 21;
static int initialPiecePos[]={186,186,0};
Player::Player(void)
{
	bank = new Piece*[4];
	totalBank=0;
	bankSelect=0;
	firstBlock=true;
	playerNumber = -1;
	endGame=false;
	lastPlacedPiece=-1;
}
Player::~Player(void)
{
	delete [] bank;
}
void Player::init(int ** pieceArray, int pieceColor, int playernum){
	//initialize pieces and piece states
	playerNumber = playernum;
	for(int i = 0; i < numPieces; i++){
		pieces.push_back(new Piece(i,pieceArray,pieceColor,initialPiecePos));
	}
	std::cout<<"player initialized"<<std::endl;
	initBank();
	std::cout<<"player bank initialized"<<std::endl;
}
void Player::initBank(void){
	bankIndex=0;
	bankSelect = 0;
}
vector<Piece*>::iterator Player::getBank(void){
	vector<Piece*>::iterator nth = pieces.begin()+bankIndex;
	return nth;
}
vector<Piece*>::iterator Player::getBankEnd(void){
	return pieces.end();
}
void Player::rotateBankUp(void){ 
	if(bankIndex > 0){
		bankIndex--;
	}
}
void Player::rotateBankDown(void){
	if(pieces.size() < 4){
		bankIndex=0;
	}
	else if(bankIndex < (signed int)pieces.size() - 5){
		bankIndex++;
	}
}
void Player::refreshBank(void){
	totalBank=0;
	for(int i = 0; i < numPieces; i++){
		if(pieces[i]->getState() == 1 || pieces[i]->getState()==2)
			totalBank ++;
	}

}
void Player::display(void){
	
}
void Player::passBankInput(signed int key){
	switch(key){
		case SDLK_DOWN:
			if(bankIndex+bankSelect >= (signed int)pieces.size()){
				bankSelect = pieces.size()-bankIndex;
			}
			else if(bankSelect == 3){
				rotateBankDown();
			}
			else{
				bankSelect++;
				if(bankIndex+bankSelect >= (signed int)pieces.size()){
					bankSelect = pieces.size()-bankIndex;
				}
			}
			break;
		case SDLK_UP:
			if(bankSelect < 1){
				rotateBankUp();
			}
			else{
				bankSelect--;
			}
			break;
	}
}
Piece * Player::getActivePiece(void){
	return pieces[bankIndex+bankSelect];
}
bool Player::removeActivePiece(void){

	vector<Piece*>::iterator it = pieces.begin()+bankIndex+bankSelect;
    lastPlacedPiece = pieces[bankIndex+bankSelect]->getPieceNumber();
	pieces.erase(it);
	return false;
}