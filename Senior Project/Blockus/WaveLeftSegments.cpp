#include "stdafx.h"
#include "WaveLeftSegments.h"

        GesturePartResult WaveLeftSegment1::CheckGesture(const NUI_SKELETON_DATA & skeleton)
        {

            // hand above elbow
            if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_LEFT].y > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_LEFT].y)
            {
                // hand right of elbow
                if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_LEFT].x > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_LEFT].x)
                {
                    return Succeed;
                }

                // hand has not dropped but is not quite where we expect it to be, pausing till next frame
                return Pausing;
            }

            // hand dropped - no gesture fails
            return Fail;
        }
         



		GesturePartResult WaveLeftSegment2::CheckGesture(const NUI_SKELETON_DATA & skeleton)
        {
            // hand above elbow
            if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_LEFT].y > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_LEFT].y)
            {
                // hand right of elbow
                if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_LEFT].x < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_LEFT].x)
                {
                    return Succeed;
                }

                // hand has not dropped but is not quite where we expect it to be, pausing till next frame
                return Pausing;
            }

            // hand dropped - no gesture fails
            return Fail;
        }

