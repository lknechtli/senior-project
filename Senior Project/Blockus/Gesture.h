#pragma once
#include "IRelativeGestureSegment.h"
#include <string>

class Gesture{
private: 
	    /// <summary>
        /// The parts that make up this gesture
        /// </summary>
		IRelativeGestureSegment * gestureParts[3];

        /// <summary>
        /// The current gesture part that we are matching against
        /// </summary>
        int currentGesturePart;// = 0;

        /// <summary>
        /// the number of frames to pause for when a pause is initiated
        /// </summary>
        int pausedFrameCount;// = 10;

        /// <summary>
        /// The current frame that we are on
        /// </summary>
        int frameCount;// = 0;

        /// <summary>
        /// Are we paused?
        /// </summary>
        bool paused;// = false;

		int length;

public:
		/// <summary>
        /// Initializes a new instance of the <see cref="Gesture"/> class.
        /// </summary>
        /// <param name="type">The type of gesture.</param>
        /// <param name="gestureParts">The gesture parts.</param>
        Gesture(GestureType type, IRelativeGestureSegment ** gestureParts, int l);

        /// <summary>
        /// Occurs when [gesture recognised].
        /// </summary>
        HANDLE GestureRecognizedEvent;

		        /// <summary>
        /// The name of gesture that this is
        /// </summary>
        GestureType type;

        /// <summary>
        /// Resets this instance.
        /// </summary>
        void Reset();
		bool GestureRecognized;

		void UpdateGesture(NUI_SKELETON_DATA data);
};