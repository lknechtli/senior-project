#include "stdafx.h"
#include "GestureController.h" 
#include <time.h>
#include <stdio.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

GestureController::GestureController()
{
	//intalize gestures
	IRelativeGestureSegment * swipeLeftSegments[3];
    swipeLeftSegments[0] = new SwipeLeftSegment1();
    swipeLeftSegments[1] = new SwipeLeftSegment2();
    swipeLeftSegments[2] = new SwipeLeftSegment3();
    
	IRelativeGestureSegment * swipeRightSegments[3];
    swipeRightSegments[0] = new SwipeRightSegment1();
    swipeRightSegments[1] = new SwipeRightSegment2();
    swipeRightSegments[2] = new SwipeRightSegment3();

	IRelativeGestureSegment * swipeUpSegments[1];
    //swipeUpSegments[2] = new SwipeUpSegment1();
    //swipeUpSegments[0] = new SwipeUpSegment2();
    swipeUpSegments[0] = new SwipeUpSegment3();

	IRelativeGestureSegment * swipeDownSegments[1];
    //swipeDownSegments[0] = new SwipeDownSegment1();
    //swipeDownSegments[1] = new SwipeDownSegment2();
    swipeDownSegments[0] = new SwipeDownSegment3();

	//Waving
	//IRelativeGestureSegment * swipeDownSegments[2];
 //   swipeDownSegments[0] = new SwipeDownSegment1();
 //   swipeDownSegments[1] = new SwipeDownSegment2();
 //   swipeDownSegments[2] = new SwipeDownSegment3();


	IRelativeGestureSegment * joinedHandsSegments[1];    
	joinedHandsSegments[0] = new JoinedHandsSegment1();
    
	IRelativeGestureSegment * leftClickSegments[3];    
	leftClickSegments[0] = new LeftClickSegment1();
   leftClickSegments[1] = new LeftClickSegment2();
   leftClickSegments[2] = new LeftClickSegment3();
    






	//gestureController.AddGesture("SwipeLeft", swipeleftSegments);
	//gesture.GestureRecognized += OnGestureRecognized;
    //this.gestures.Add(gesture);

	Gesture * gestureLeftSwipe = new Gesture(LeftSwipe, swipeLeftSegments,SwipeLeftLength);
	Gesture * gestureRightSwipe = new Gesture(RightSwipe, swipeRightSegments,SwipeRightLength);
	Gesture * gestureUpSwipe = new Gesture(UpSwipe, swipeUpSegments,SwipeUpLength);
	Gesture * gestureDownSwipe = new Gesture(DownSwipe, swipeDownSegments,SwipeDownLength);
	Gesture * gestureJoinedHands = new Gesture(JoinedHands, joinedHandsSegments,1);
	Gesture * gestureLeftClick = new Gesture(LeftClick, leftClickSegments,LeftClickLength);

	gestures[0]=gestureLeftSwipe;
	gestures[1]=gestureRightSwipe;
	gestures[2]=gestureUpSwipe;
	gestures[3]=gestureDownSwipe;
	gestures[4]=gestureJoinedHands;
gestures[5]=gestureLeftClick;

    
}

void GestureController::UpdateAllGestures(NUI_SKELETON_DATA data)
{
	static clock_t lastUp = clock();
	static clock_t lastDown = clock();
	static clock_t lastSkip = clock();

	for(Gesture * g: gestures){
		g->UpdateGesture(data);
		if(g->GestureRecognized){
			
			SDL_Event gesture_event;
			


			gesture_event.type=SDL_KEYDOWN;
			clock_t currtime = clock();
			float diff1 = (((float)currtime - (float)lastUp) / CLOCKS_PER_SEC ) * 1000;
			float diff2 = (((float)currtime - (float)lastDown) / CLOCKS_PER_SEC ) * 1000;
			float diff3 = (((float)currtime - (float)lastSkip) / CLOCKS_PER_SEC ) * 1000;

				switch(g->type){
					case UpSwipe:
						//clock_t currtime = clock();
						
						if(diff1 > 500){
							lastUp = clock();
							gesture_event.key.keysym.sym=SDLK_UP;
							std::cout<<"gesture: UpSwipe"<<std::endl;
						}
						break;
					case DownSwipe:
						//clock_t currtime = clock();
						
						if(diff2 > 500){
							lastDown = clock();
							gesture_event.key.keysym.sym=SDLK_DOWN;
							std::cout<<"gesture: DownSwipe"<<std::endl;
						}
						break;
					case RightSwipe:
						gesture_event.key.keysym.sym=SDLK_r; 
						std::cout<<"gesture: RightSwipe"<<std::endl;
						break;
					case LeftSwipe:
						gesture_event.key.keysym.sym=SDLK_ESCAPE; 
						std::cout<<"gesture: LeftSwipe"<<std::endl;
						break;
					case WaveLeft:
						gesture_event.key.keysym.sym=SDLK_x; 
						std::cout<<"gesture: WaveLeft"<<std::endl;
						break;
					case JoinedHands:
						if(diff3 > 100000){
							lastSkip = clock();
							//gesture_event.key.keysym.sym=SDLK_s; 
							std::cout<<"gesture: JoinedHands"<<std::endl;
							break;
						}
					case LeftClick:
						gesture_event.key.keysym.sym=SDLK_SPACE; 
						std::cout<<"gesture: LeftClick"<<std::endl;
						break;
					/*case eeHandGestureDepthBackToFront:
						; break;*/
				}
				//cout<<"eeHandGestureHorizontalRightToLeft"<<endl;
				//handleKeyEvents(&gesture_event);



				
				SDL_PushEvent(&gesture_event);



			g->GestureRecognized=false;
		}
	}

} 