#pragma once

enum GesturePartResult
{
        /// <summary>
        /// Gesture part fail
        /// </summary>
        Fail,

        /// <summary>
        /// Gesture part succeed
        /// </summary>
        Succeed,

        /// <summary>
        /// Gesture part result undetermined
        /// </summary>
        Pausing
};

enum GestureType 
{
        /// <summary>
        /// Waved with right hand
        /// </summary>
        WaveRight,

        /// <summary>
        /// Waved with the left hand
        /// </summary>
        WaveLeft,

        /// <summary>
        /// Asked for the menu
        /// </summary>
        Menu,

        /// <summary>
        /// Swiped left
        /// </summary>
        LeftSwipe,

        /// <summary>
        /// swiped right
        /// </summary>
        RightSwipe,

		UpSwipe,

		DownSwipe,

		JoinedHands,

		LeftClick
};