#include "stdafx.h"
#include "LeftClickSegments.h"
#include <math.h>

        GesturePartResult LeftClickSegment1::CheckGesture(const NUI_SKELETON_DATA & skeleton)
        {
			//ensure orientation as well as andle
			Vector3d upperArm;
			upperArm.x=skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_LEFT].x-skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_LEFT].x;
			upperArm.y=skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_LEFT].y-skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_LEFT].y;
			upperArm.z=skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_LEFT].z-skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_LEFT].z;
			Vector3d foreArm;
			foreArm.x=skeleton.SkeletonPositions[NUI_SKELETON_POSITION_WRIST_LEFT].x-skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_LEFT].x;
			foreArm.y=skeleton.SkeletonPositions[NUI_SKELETON_POSITION_WRIST_LEFT].y-skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_LEFT].y;
			foreArm.z=skeleton.SkeletonPositions[NUI_SKELETON_POSITION_WRIST_LEFT].z-skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_LEFT].z;
			
			double theta = acos((upperArm.dot(foreArm))/(upperArm.magnitude()*foreArm.magnitude()))*(180/PI);
			
			if(theta<90){
				//std::cout<<"LCLICK 1 theta: "<<theta<<std::endl;

				return Succeed;
			
			}
			return Fail;

            // LEFT WRIST in front of LEFT shoulder
            //if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_WRIST_LEFT].z < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_LEFT].z && skeleton.SkeletonPositions[NUI_SKELETON_POSITION_WRIST_LEFT].y < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_CENTER].y)
            //{
            //    // LEFT WRIST below head height and WRIST higher than elbow
            //    if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_WRIST_LEFT].y < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HEAD].y && skeleton.SkeletonPositions[NUI_SKELETON_POSITION_WRIST_LEFT].y > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_LEFT].y)
            //    {
            //        // LEFT WRIST LEFT of LEFT shoulder
            //        if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_WRIST_LEFT].x > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_LEFT].x)
            //        {
            //            return Succeed;
            //        }
            //        return Pausing;
            //    }
            //    return Fail;
            //}
            //return Fail;
        }
         



		GesturePartResult LeftClickSegment2::CheckGesture(const NUI_SKELETON_DATA & skeleton)
        {
			Vector3d upperArm;
			upperArm.x=skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_LEFT].x-skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_LEFT].x;
			upperArm.y=skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_LEFT].y-skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_LEFT].y;
			upperArm.z=skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_LEFT].z-skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_LEFT].z;
			Vector3d foreArm;
			foreArm.x=skeleton.SkeletonPositions[NUI_SKELETON_POSITION_WRIST_LEFT].x-skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_LEFT].x;
			foreArm.y=skeleton.SkeletonPositions[NUI_SKELETON_POSITION_WRIST_LEFT].y-skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_LEFT].y;
			foreArm.z=skeleton.SkeletonPositions[NUI_SKELETON_POSITION_WRIST_LEFT].z-skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_LEFT].z;
			

			double theta = acos((upperArm.dot(foreArm))/(upperArm.magnitude()*foreArm.magnitude()))*(180/PI);
			//std::cout<<"2theta "<<theta<<std::endl;
			
			if(theta>45){
				if(theta>160){
					//std::cout<<"LCLICK 2 theta: "<<theta<<std::endl;
					return Succeed;
				}
				return Pausing;
			}
			return Fail;
        }

				GesturePartResult LeftClickSegment3::CheckGesture(const NUI_SKELETON_DATA & skeleton)
        {
			Vector3d upperArm;
			upperArm.x=skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_LEFT].x-skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_LEFT].x;
			upperArm.y=skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_LEFT].y-skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_LEFT].y;
			upperArm.z=skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_LEFT].z-skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_LEFT].z;
			Vector3d foreArm;
			foreArm.x=skeleton.SkeletonPositions[NUI_SKELETON_POSITION_WRIST_LEFT].x-skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_LEFT].x;
			foreArm.y=skeleton.SkeletonPositions[NUI_SKELETON_POSITION_WRIST_LEFT].y-skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_LEFT].y;
			foreArm.z=skeleton.SkeletonPositions[NUI_SKELETON_POSITION_WRIST_LEFT].z-skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_LEFT].z;
			

			double theta = acos((upperArm.dot(foreArm))/(upperArm.magnitude()*foreArm.magnitude()))*(180/PI);
			///std::cout<<"3theta "<<theta<<std::endl;

			if(theta<190){
				if(theta<90){
					//std::cout<<"LCLICK 3 theta: "<<theta<<std::endl;					
					return Succeed;
				}
				return Pausing;
			}
			return Fail;
        }