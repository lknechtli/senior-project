#include "stdafx.h"
#include "JoinedHandsSegment.h"
using namespace std;
        GesturePartResult JoinedHandsSegment1::CheckGesture(const NUI_SKELETON_DATA & skeleton)
		{
			// Right and Left Hand in front of Shoulders
            if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_LEFT].z < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_LEFT].z && skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].z < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_RIGHT].z)
            {
                // Hands between shoulder and hip
                if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].y < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_CENTER].y && skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].y > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HIP_CENTER].y &&
                    skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_LEFT].y < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_CENTER].y && skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_LEFT].y > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HIP_CENTER].y)
                {
                    // Hands between shoulders
                    if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].x < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_RIGHT].x && skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].x > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_LEFT].x &&
                        skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_LEFT].x > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_LEFT].x && skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_LEFT].x < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_RIGHT].x)
                    {
                        // Hands very close
                        if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].x - skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_LEFT].x < 0)
                        {
                            return Succeed;
                        }
						//cout<<"hands not very close"<<endl;
                        return Pausing;
                    }
					//cout<<"hands not between shoulders"<<endl;
                    return Fail;
                }
				//cout<<"hands not between shoulders and hip"<<endl;
                return Fail;
            }
			//cout<<"hands not in fron of shoulders"<<endl;
            return Fail;
        }


