#pragma once
 
#include "stdafx.h"
#include "IRelativeGestureSegment.h"
#include "GestureEnumTypes.h"

#define SwipeUpLength 1

    class SwipeUpSegment1 : public IRelativeGestureSegment
    {
        /// <summary>
        /// Checks the gesture.
        /// </summary>
        /// <param name="skeleton">The skeleton.</param>
        /// <returns>GesturePartResult based on if the gesture part has been completed</returns>
		public:
			GesturePartResult CheckGesture(const NUI_SKELETON_DATA & skeleton);
    };


    class SwipeUpSegment2 : public IRelativeGestureSegment
    {
        /// <summary>
        /// Checks the gesture.
        /// </summary>
        /// <param name="skeleton">The skeleton.</param>
        /// <returns>GesturePartResult based on if the gesture part has been completed</returns>
		public:
			GesturePartResult CheckGesture(const NUI_SKELETON_DATA & skeleton);
    };

	
    class SwipeUpSegment3 : public IRelativeGestureSegment
    {
        /// <summary>
        /// Checks the gesture.
        /// </summary>
        /// <param name="skeleton">The skeleton.</param>
        /// <returns>GesturePartResult based on if the gesture part has been completed</returns>
		public:
			GesturePartResult CheckGesture(const NUI_SKELETON_DATA & skeleton);
    };