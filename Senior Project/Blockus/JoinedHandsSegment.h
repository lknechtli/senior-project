#pragma once
 
#include "IRelativeGestureSegment.h"
#include "GestureEnumTypes.h"


    class JoinedHandsSegment1 : public IRelativeGestureSegment
    {
        /// <summary>
        /// Checks the gesture.
        /// </summary>
        /// <param name="skeleton">The skeleton.</param>
        /// <returns>GesturePartResult based on if the gesture part has been completed</returns>
		public:
			GesturePartResult CheckGesture(const NUI_SKELETON_DATA & skeleton);
    };