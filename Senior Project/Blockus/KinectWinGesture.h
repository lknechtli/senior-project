#ifndef __KINECTWINGESTURE_H__
#define __KINECTWINGESTURE_H__
#include "KinectWinLibCmn.h"
#include <vector>


typedef struct _NuiTrackedSkeletalObjectLocation
{
    float x;
    float y;
	float actualx;
	float actualy;
	float actualz;

} NuiTrackedSkeletalObjectLocation;

typedef enum _eHandGestureType
{
    eHandGestureNone = 0,
    eeHandGestureHorizontalLeftToRight,
    eeHandGestureHorizontalRightToLeft,
    eeHandGestureVerticalTopToBottom,
    eeHandGestureVerticalBottomToTop,
	eeHandGestureDepthFrontToBack,
	eeHandGestureDepthBackToFront
} eHandGestureType;

class KinectWinGesture
{
public:
    KinectWinGesture();
    ~KinectWinGesture();
    eHandGestureType GuessHandGuesture();
    void SaveHandNuiPosition( float x, float y, float actualx, float actualy, float actualz);


private:
    CRITICAL_SECTION m_csNuiHandLocationHistory;
    std::vector<NuiTrackedSkeletalObjectLocation> m_NuiHandLocationHistory;
};

#endif // __KINECTWINGESTURE_H__