#pragma once
#include "GestureEnumTypes.h"
#include <windows.h>
#include <ole2.h>
#include <NuiApi.h>


class IRelativeGestureSegment
{
	public: 
        /// <summary>
        /// Checks the gesture.
        /// </summary>
        /// <param name="skeleton">The skeleton.</param>
        /// <returns>GesturePartResult based on if the gesture part has been completed</returns>
        virtual GesturePartResult CheckGesture(const NUI_SKELETON_DATA & skeleton)=0;
};
