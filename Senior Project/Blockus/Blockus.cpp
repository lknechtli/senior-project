// Blockus.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <math.h>
#include <iostream>
#include <stdlib.h>
#include "Board.h"
#include "Engine.h"

#include <windows.h>
#include <ole2.h>

#include <NuiApi.h>
#include <NuiImageCamera.h>
#include <NuiSensor.h>
#include <assert.h>
#include <D2DBaseTypes.h>
#include <D2D1helper.h>
#include "KinectWinGesture.h"
#include "GestureController.h"

#define width 640
#define height 480
typedef D2D_POINT_2F D2D1_POINT_2F;
// OpenGL Variables
GLuint textureId;
GLubyte data[width*height*4];

// Kinect variables
HANDLE m_pSkeletonStreamHandle;
HANDLE m_hNextSkeletonEvent;
HANDLE rgbStream;
INuiSensor* sensor;
HRESULT hr;
NUI_SKELETON_FRAME skeletonFrame;

// SDL Variables
// Kinect Window
SDL_Surface* kScreen;
SDL_Window* kWindow;
SDL_GLContext glcontext; 
//Game Window
SDL_Window *displayWindow = NULL;
SDL_Surface *displaySurface = NULL;
SDL_Renderer *displayRenderer = NULL;

// Skeletal drawing
D2D1_POINT_2F m_Points[NUI_SKELETON_POSITION_COUNT];
static const int        cScreenWidth  = 320;
static const int        cScreenHeight = 240;
static const float g_JointThickness = 3.0f;
static const float g_TrackedBoneThickness = 6.0f;
static const float g_InferredBoneThickness = 1.0f;

//Kinect Gestures
HANDLE m_hNuiProcessThread;
HANDLE m_hNuiGuessGestureThread;
HANDLE m_hEvNuiProcessThreadStopEvent;
KinectWinGesture m_kinectWinGesture;
GestureController * gestureController;



Board *gameBoard;
//constants
static int const ScreenW = 580, ScreenH = 500;
enum{BANKMODE=1,PLACEMODE};


static void quit(int code){ //make way to exit cleanly
	SDL_Quit();
	exit(code);
}
static void handleKeyEvents(SDL_Event * event){
	auto keyPressed = event->key.keysym.sym;
	switch(keyPressed){
	case SDLK_q:
		quit(0); break;
	default:
		gameBoard->passInput(keyPressed); break;
	}
}
bool initKinectWindow(int argc, char* argv[]) {
	kWindow = SDL_CreateWindow("Kinect Interaction", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height,  SDL_WINDOW_OPENGL);
	return kWindow;
}
bool initGameWindow(int argc, char* argv[]) {
	displayWindow = SDL_CreateWindow("Blockus", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, ScreenW, ScreenH, SDL_WINDOW_SHOWN);
	return displayWindow;
}
bool initSDLWindows(int argc, char* argv[]){
	SDL_Init(SDL_INIT_EVERYTHING);
	return (initKinectWindow(argc, argv) && initGameWindow(argc, argv));
}
bool initOpenGL(){
	glcontext = SDL_GL_CreateContext(kWindow);
	glClearColor(0,0,0,1);
	glClear(GL_COLOR_BUFFER_BIT);
	SDL_GL_SwapWindow(kWindow);

	// Initialize textures
	glGenTextures(1, &textureId);
	glBindTexture(GL_TEXTURE_2D, textureId);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, (GLvoid*) data);
    glBindTexture(GL_TEXTURE_2D, 0);

    // OpenGL setup
	glClearColor(0,0,0,1);
	glClearDepth(1.0f);
	glEnable(GL_TEXTURE_2D);

    // Camera setup
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, width, height, 0, 1, -1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	return 1;
}



// Thread that guesses gesture at regular intervals.
DWORD WINAPI NuiGuessGestureThread( LPVOID pParam )
{
    //KINECTWIN_FN_ENTRY;
    int nWaitRetCode = 0;
    eHandGestureType detectedHandGuesture = eHandGestureNone;
    //KinectWinMgr* pThis = (KinectWinMgr*)pParam;
    while( 1 )
    {
        nWaitRetCode = WaitForSingleObject( m_hEvNuiProcessThreadStopEvent, KINECTWINLIB_NUI_GESTURE_WAIT_TIMEOUT_IN_MS );
        if( WAIT_OBJECT_0 == nWaitRetCode )
        {
            // Exit from loop
            SetEvent(m_hEvNuiProcessThreadStopEvent );
            break;
        }
        else if( WAIT_TIMEOUT == nWaitRetCode )
        {
   //         // Check for any guesture every second?
   //         detectedHandGuesture = m_kinectWinGesture.GuessHandGuesture();
			//
			//if(detectedHandGuesture!=eHandGestureNone){
			//	SDL_Event gesture_event;
			//	//event->key.keysym.sym
			//	//SDLK_DOWN,SDLK_UP,SDLK_SPACE,SDLK_ESCAPE
			//	switch(detectedHandGuesture){
			//		case eeHandGestureHorizontalLeftToRight:
			//			cout<<"eeHandGestureHorizontalLeftToRight"<<endl;
			//			gesture_event.key.keysym.sym=SDLK_ESCAPE;
			//			break;
			//		case eeHandGestureHorizontalRightToLeft:
			//			cout<<"eeHandGestureHorizontalRightToLeft"<<endl;
			//			gesture_event.key.keysym.sym=SDLK_SPACE;
			//			break;
			//		case eeHandGestureVerticalTopToBottom:
			//			cout<<"eeHandGestureVerticalTopToBottom"<<endl;
			//			gesture_event.key.keysym.sym=SDLK_DOWN; 
			//			break;
			//		case eeHandGestureVerticalBottomToTop:
			//			cout<<"eeHandGestureVerticalBottomToTop"<<endl;
			//			gesture_event.key.keysym.sym=SDLK_UP; 
			//			break;
			//		case eeHandGestureDepthFrontToBack:
			//			; break;
			//		case eeHandGestureDepthBackToFront:
			//			; break;
			//	}
			//	handleKeyEvents(&gesture_event);



			//	
			//	SDL_PushEvent(&gesture_event);
			//	
			//	BOOL HORRAY=TRUE;
			//
			//}

   //         //SendKeyboardInputs( detectedHandGuesture );
        }
        else
        {
//#ifdef KINECTWIN_DBG_LOG
//            OutputDebugString( "WaitForSingleObject error!" );
//#endif // KINECTWIN_DBG_LOG
//            break;
        }
    }
    //KINECTWIN_FN_EXIT;
    return 0;
}


bool rightHandInMouseSquare(float x, float y){
	if(	x>180-50 && x<280+50 && y>40-50 && y<140+50){
		//cout << 1 <<endl;
		return true;
	}
	else{
		//cout << 0 << " x: " <<x<<" y: "<<y<<endl;  
		return false;
	}
}

D2D1_POINT_2F SkeletonToScreen(Vector4 skeletonPoint, int widthk, int heightk){
	LONG x, y;
    USHORT depth;

    // Calculate the skeleton's position on the screen
    // NuiTransformSkeletonToDepthImage returns coordinates in NUI_IMAGE_RESOLUTION_320x240 space
    NuiTransformSkeletonToDepthImage(skeletonPoint, &x, &y, &depth);

    float screenPointX = static_cast<float>(x * width) / cScreenWidth;
    float screenPointY = static_cast<float>(y * height) / cScreenHeight;

    return D2D1::Point2F(screenPointX, screenPointY);
}

void processSkeletons(){
    // ELN this could be a problem, need to zero this out.
	//skeletonFrame = {0};
    //NUI_SKELETON_FRAME nuiSkeletonFrame = {0};
	
	
	HRESULT hr = sensor->NuiSkeletonGetNextFrame(0, &skeletonFrame);
    if ( FAILED(hr) )
    {
		perror("NuiSkeletonGetNextFrame");
        return;
    }

	// Check if we have any skeleton
    int i = 0;
    bool bFoundSkeleton = false;
    for( i = 0; i < NUI_SKELETON_COUNT; i++ )
    {
        // Found atleast one tracked skeleton
        if( skeletonFrame.SkeletonData[i].eTrackingState == NUI_SKELETON_TRACKED )
        {
            bFoundSkeleton = true;
            break;
        }
    }


	if( bFoundSkeleton )
    {
        // Smooth the skeleton positions
        NUI_TRANSFORM_SMOOTH_PARAMETERS nuiSmoothParams;
        nuiSmoothParams.fCorrection = 0.3f;
        nuiSmoothParams.fJitterRadius = 1.0f;
        nuiSmoothParams.fMaxDeviationRadius = 0.5f;
        nuiSmoothParams.fPrediction = 0.4f;
        nuiSmoothParams.fSmoothing = 0.7f;
        NuiTransformSmooth( &skeletonFrame, &nuiSmoothParams );
		//important
		
		gestureController->UpdateAllGestures(skeletonFrame.SkeletonData[i]);
        
		// Convert left hand vector4 to x,y coordinates
        float fLeftX = 0, fLeftY = 0;
        Vector4 v4LeftHand = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_HAND_LEFT];
        NuiTransformSkeletonToDepthImage( v4LeftHand, &fLeftX, &fLeftY );
		m_kinectWinGesture.SaveHandNuiPosition( fLeftX, fLeftY, v4LeftHand.x ,v4LeftHand.y,v4LeftHand.z );


		float fRightX = 0, fRightY = 0;
        Vector4 v4RightHand = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT];
        NuiTransformSkeletonToDepthImage( v4RightHand, &fRightX, &fRightY );
		
		//cout<<"try "<<fRightX<<" "<<fRightY<<endl;
		
		//If the board is in placing mode and the right hand is in the mouse square transform the coordinates to the game space
		if(gameBoard->isPlacingPiece() && rightHandInMouseSquare(fRightX, fRightY)){
			//Transform from 100x100 from x->180-280 & y->40-140 to 320x240 
			int row,col;
			row = (int)(fRightX-180)/5;
			col = (int)(fRightY-40)/5;
			gameBoard->movePiece(row,col);
		}
		//cout << rightHandInMouseSquare(fRightX, fRightY)<<endl;
		//rightHandInMouseSquare(fRightX, fRightY);

        // Move cursor
        //SendMouseInputs( fLeftX, fLeftY );

        // Save coordinates
    }

    // smooth out the skeleton data
    //sensor->NuiTransformSmooth(&skeletonFrame, NULL);
}

// Thread that processes events from NUI runtime
DWORD WINAPI NuiProcessThread( LPVOID pParam )
{
    //KINECTWIN_FN_ENTRY;
    //KinectWinMgr* pThis = (KinectWinMgr*)pParam;
    HANDLE hEvents[2];
    int nEventIdx = 0;

    // Configure events to be listened on
    hEvents[0] = m_hEvNuiProcessThreadStopEvent;
    hEvents[1] = m_hNextSkeletonEvent;

    // Main thread loop
    while(1)
    {
        // Wait for an event to be signalled
        nEventIdx = WaitForMultipleObjects( sizeof(hEvents)/sizeof(hEvents[0]), hEvents, FALSE, KINECTWINLIB_NUI_WAIT_TIMEOUT_IN_MS );
        if( 0 == ( WAIT_OBJECT_0 + nEventIdx ) )
        {
            // Exit from loop
            SetEvent(m_hEvNuiProcessThreadStopEvent );
            break;
        }
        else if( 1 == ( WAIT_OBJECT_0 + nEventIdx ) )
        {
            // Process skeleton motion alert
			processSkeletons();
        }
        else if( WAIT_FAILED == nEventIdx )
        {
            break;
        }
    }
    return 0;
}


bool initKinect() {
	// ELN TODO: add error schecking like microsoft samples have
	// Get a working kinect sensor
	
	int numSensors;
	INuiSensor * possibleSensor;

	hr = NuiGetSensorCount(&numSensors);
	if (FAILED(hr))
    {
        return false;
    }
	
	hr = NuiCreateSensorByIndex(0, &possibleSensor);
	if (FAILED(hr))
    {
        return false;
    }

	hr = possibleSensor->NuiStatus();
    if (S_OK == hr)
    {
		sensor = possibleSensor;
    }
	else
	{
		possibleSensor->Release();
		return false;
	}

    // Initialize the Kinect and specify that we'll be using skeleton
    hr = sensor->NuiInitialize(NUI_INITIALIZE_FLAG_USES_DEPTH | NUI_INITIALIZE_FLAG_USES_COLOR | NUI_INITIALIZE_FLAG_USES_SKELETON ); 
    if (SUCCEEDED(hr))
    {
		// Create an event that will be signaled when skeleton data is available
        m_hNextSkeletonEvent = CreateEventW(NULL, TRUE, FALSE, NULL);
		
		// Open a skeleton stream to receive skeleton data and an image screen for rgb data
		//maybe check these operations
        sensor->NuiSkeletonTrackingEnable(m_hNextSkeletonEvent, 0); 
		sensor->NuiImageStreamOpen(NUI_IMAGE_TYPE_COLOR, NUI_IMAGE_RESOLUTION_640x480,0,2,NULL,&rgbStream); 
    }

    if (NULL == sensor)
    {
       perror ("No ready Kinect found");
       return false;
    }

	// Start the NUI processing thread
    m_hEvNuiProcessThreadStopEvent = CreateEvent( NULL, FALSE, FALSE, NULL );
	m_hNuiProcessThread = CreateThread( NULL, 0, NuiProcessThread, /*this*/NULL, 0, NULL );
    m_hNuiGuessGestureThread = CreateThread( NULL, 0, NuiGuessGestureThread, /*this*/NULL, 0, NULL );

    return sensor;
}



//Draw the mouse square
void DrawMouseSquare(){
	glColor3f(1.0f, 0.0f, 0.0f);
	glLineWidth(g_TrackedBoneThickness);
	glBegin(GL_LINE_LOOP);
		glVertex2i(360, 80);
		glVertex2i(560, 80);
		glVertex2i(560, 280);
		glVertex2i(360, 280);
	glEnd();
	glColor3f(1.0f, 1.0f, 1.0f);
} 


void DrawBone(const NUI_SKELETON_DATA & skel, NUI_SKELETON_POSITION_INDEX joint0, NUI_SKELETON_POSITION_INDEX joint1)
{
    NUI_SKELETON_POSITION_TRACKING_STATE joint0State = skel.eSkeletonPositionTrackingState[joint0];
    NUI_SKELETON_POSITION_TRACKING_STATE joint1State = skel.eSkeletonPositionTrackingState[joint1];

    // If we can't find either of these joints, exit
    if (joint0State == NUI_SKELETON_POSITION_NOT_TRACKED || joint1State == NUI_SKELETON_POSITION_NOT_TRACKED)
    {
        return;
    }

    // Don't draw if both points are inferred
    if (joint0State == NUI_SKELETON_POSITION_INFERRED && joint1State == NUI_SKELETON_POSITION_INFERRED)
    {
        return;
    }

    // We assume all drawn bones are inferred unless BOTH joints are tracked
    if (joint0State == NUI_SKELETON_POSITION_TRACKED && joint1State == NUI_SKELETON_POSITION_TRACKED)
    {
		glColor3f(1.0f, 0.0f, 0.0f);
		glLineWidth(g_TrackedBoneThickness);
		glBegin(GL_LINES);
		glVertex2i(m_Points[joint0].x, m_Points[joint0].y);
		glVertex2i(m_Points[joint1].x, m_Points[joint1].y);
		glEnd();
    }
    else
    {
		glColor3f(0.0f, 0.0f, 1.0f);
		glLineWidth(g_InferredBoneThickness);
		glBegin(GL_LINES);
		glVertex2i(m_Points[joint0].x, m_Points[joint0].y);
		glVertex2i(m_Points[joint1].x, m_Points[joint1].y);
		glEnd();
    }
}
void drawJoints(const NUI_SKELETON_DATA & skel)
{
    glBegin(GL_POINTS);
	glPointSize(g_JointThickness);
	glColor3f(0.0f, 0.0f, 1.0f);
	for (int i = 0; i < NUI_SKELETON_POSITION_COUNT; ++i)
    {
        if (skel.eSkeletonPositionTrackingState[i] == NUI_SKELETON_POSITION_INFERRED )
        {
			glVertex2i(m_Points[i].x, m_Points[i].y);
        }
        else if ( skel.eSkeletonPositionTrackingState[i] == NUI_SKELETON_POSITION_TRACKED )
        {
			glVertex2i(m_Points[i].x, m_Points[i].y);
        }
    }
	glEnd();
	glColor3f(1.0f, 1.0f, 1.0f);
	
}
void drawSkeleton(const NUI_SKELETON_DATA & skel, int windowWidth, int windowHeight){
	// for the moment just place dots where the joints are
	int i;
	for (i = 0; i < NUI_SKELETON_POSITION_COUNT; ++i)
    {
        m_Points[i] = SkeletonToScreen(skel.SkeletonPositions[i], windowWidth, windowHeight);
    }

	// Render Torso
    DrawBone(skel, NUI_SKELETON_POSITION_HEAD, NUI_SKELETON_POSITION_SHOULDER_CENTER);
    DrawBone(skel, NUI_SKELETON_POSITION_SHOULDER_CENTER, NUI_SKELETON_POSITION_SHOULDER_LEFT);
    DrawBone(skel, NUI_SKELETON_POSITION_SHOULDER_CENTER, NUI_SKELETON_POSITION_SHOULDER_RIGHT);
    DrawBone(skel, NUI_SKELETON_POSITION_SHOULDER_CENTER, NUI_SKELETON_POSITION_SPINE);
    DrawBone(skel, NUI_SKELETON_POSITION_SPINE, NUI_SKELETON_POSITION_HIP_CENTER);
    DrawBone(skel, NUI_SKELETON_POSITION_HIP_CENTER, NUI_SKELETON_POSITION_HIP_LEFT);
    DrawBone(skel, NUI_SKELETON_POSITION_HIP_CENTER, NUI_SKELETON_POSITION_HIP_RIGHT);

    // Left Arm
    DrawBone(skel, NUI_SKELETON_POSITION_SHOULDER_LEFT, NUI_SKELETON_POSITION_ELBOW_LEFT);
    DrawBone(skel, NUI_SKELETON_POSITION_ELBOW_LEFT, NUI_SKELETON_POSITION_WRIST_LEFT);
    DrawBone(skel, NUI_SKELETON_POSITION_WRIST_LEFT, NUI_SKELETON_POSITION_HAND_LEFT);

    // Right Arm
    DrawBone(skel, NUI_SKELETON_POSITION_SHOULDER_RIGHT, NUI_SKELETON_POSITION_ELBOW_RIGHT);
    DrawBone(skel, NUI_SKELETON_POSITION_ELBOW_RIGHT, NUI_SKELETON_POSITION_WRIST_RIGHT);
    DrawBone(skel, NUI_SKELETON_POSITION_WRIST_RIGHT, NUI_SKELETON_POSITION_HAND_RIGHT);

    // Left Leg
    DrawBone(skel, NUI_SKELETON_POSITION_HIP_LEFT, NUI_SKELETON_POSITION_KNEE_LEFT);
    DrawBone(skel, NUI_SKELETON_POSITION_KNEE_LEFT, NUI_SKELETON_POSITION_ANKLE_LEFT);
    DrawBone(skel, NUI_SKELETON_POSITION_ANKLE_LEFT, NUI_SKELETON_POSITION_FOOT_LEFT);

    // Right Leg
    DrawBone(skel, NUI_SKELETON_POSITION_HIP_RIGHT, NUI_SKELETON_POSITION_KNEE_RIGHT);
    DrawBone(skel, NUI_SKELETON_POSITION_KNEE_RIGHT, NUI_SKELETON_POSITION_ANKLE_RIGHT);
    DrawBone(skel, NUI_SKELETON_POSITION_ANKLE_RIGHT, NUI_SKELETON_POSITION_FOOT_RIGHT);

	// Draw the joints in a different color
	drawJoints(skel);
}
void drawSkeltons(){
	//account for changing dimensions
	//int width = rct.right;
    //int height = rct.bottom;
	int i;
	for (i = 0 ; i < NUI_SKELETON_COUNT; ++i)
    {
        NUI_SKELETON_TRACKING_STATE trackingState = skeletonFrame.SkeletonData[i].eTrackingState;

        if (NUI_SKELETON_TRACKED == trackingState)
        {
            // We're tracking the skeleton, draw it
            drawSkeleton(skeletonFrame.SkeletonData[i], width, height);
        }
    }
}

//void updateSkeletons()
//{
//    //if (NULL == sensor)
//    //{
//    //    return;
//    //}
//    // Wait for 0ms, just quickly test if it is time to process a skeleton
//    if ( WAIT_OBJECT_0 == WaitForSingleObject(m_hNextSkeletonEvent, 0) )
//    {
//        processSkeletons();
//    }
//}
void drawRGBStream(){
	glBindTexture(GL_TEXTURE_2D, textureId);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_BGRA, GL_UNSIGNED_BYTE, (GLvoid*)data);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(0, 0, 0);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(width, 0, 0);
		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(width, height, 0.0f);
		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(0, height, 0.0f);
    glEnd();
}
void updateRGBStream(GLubyte* dest) {
	NUI_IMAGE_FRAME imageFrame;
	NUI_LOCKED_RECT LockedRect;
	if (sensor->NuiImageStreamGetNextFrame(rgbStream, 0, &imageFrame) < 0) return;
	INuiFrameTexture* texture = imageFrame.pFrameTexture;
    texture->LockRect(0, &LockedRect, NULL, 0);
    if (LockedRect.Pitch != 0)
    {
        const BYTE* curr = (const BYTE*) LockedRect.pBits;
        const BYTE* dataEnd = curr + (width*height)*4;

		while (curr < dataEnd) {
			*dest++ = *curr++;
		}
    }
    texture->UnlockRect(0);
    sensor->NuiImageStreamReleaseFrame(rgbStream, &imageFrame);
}
void updateDisplay() {
	//trying to fix coloring issue
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	updateRGBStream(data);
	//updateSkeletons();
	drawRGBStream();
	drawSkeltons();
	DrawMouseSquare();
	SDL_GL_SwapWindow(kWindow);
}
int main(int argc, char* argv[])
{
	gestureController=new GestureController();
	bool kinectFail=false;
	if (!initSDLWindows(argc, argv))
		return 1;
	if (!initKinect()){
		kinectFail=true;
		//return 2;
	}
	if (!initOpenGL()) return 3;

	gameBoard = new Board();
	Engine gameEngine;
	gameEngine.loadFiles();
	SDL_Event event;
	gameBoard->setMode(PLACEMODE);

	while(true){
		//must find way to exit game using this so SDL de-allocates cleanly
		//Does the SDL_PollEvent capture all events (aka Kinect Skeletal events), should I create a seperate thread for that interaction to prevent overlap
		if(SDL_PollEvent(&event)){
			switch(event.type){
				case SDL_QUIT:
					quit(0); break;
				case SDL_KEYDOWN:
					handleKeyEvents(&event); break;
			}
		}
		if(!kinectFail){
			updateDisplay();	
		}
		

		gameEngine.setSurface(SDL_GetWindowSurface(displayWindow));
		//gameBoard->display(); //calls display method of gameboard, which can change board state only. test method
		if(!(gameBoard->gameOver())){
			gameEngine.displayBoard(gameBoard->getBoard()); //paints board pieces to surface
			//then paint piece bank to screen (call through board.activePlayer)
			gameEngine.displayBank(gameBoard->getActivePlayer());
			gameEngine.displayPlayer(gameBoard->getActivePlayer(),gameBoard->getPlayerScore());
			if(gameBoard->isPlacingPiece()){
				Piece * activePiece = gameBoard->getActivePlayer()->getActivePiece();
				int activeX = gameBoard->getActivePieceX(); 
				int activeY = gameBoard->getActivePieceY();
				gameEngine.displayActivePiece(activePiece,activeX,activeY);
			}
		}
		SDL_UpdateWindowSurface(displayWindow);
		/***
			draw scene:
			call draw methods for every object on screen
		*/
	}
	SDL_FreeSurface(displaySurface);
	//SDL_DestroyTexture(displayTexture);
	SDL_DestroyWindow(displayWindow);
	return 0;
}