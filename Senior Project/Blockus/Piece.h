#pragma once

class Piece
{
public:
	Piece(int pieceNumber, int ** pieceArray, int pieceColor, int * piecePosition);
	void move(int pos[3]);
	void display(void);
	void rotate(int dir);
	void setState(int st){if(st>=0 && st<=2){	state = st;}else{std::cerr<<"Invalid state set: "<<st<<std::endl;}};
	int getState(void){return state;} //0 is in bank, 1 is active, 2 is placed
	void setColor(int col){color=col;}
	int getColor(void){return color;}
	int * getPosition(void){return position;}
	bool setPosition(int x, int y);
	int ** getShape(void){return shape;}
	int getPieceNumber(){return pieceNumber;};
	~Piece(void);
private:

	int color; //color is opengl format - 3 floats (F,F,F) in an array, 4th value is alpha
	int *position; //(x,y) 
	int state; //0 = inactive (not displayed), 1 = active (displayed in bank), 2 = placing
	int pieceNumber;
	/*
		shape key:
		0 = no block
		1 = block
		2 = centerpoint
		5 x 5 grid
	*/
	int ** shape; 

};

