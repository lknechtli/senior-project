#pragma once
 
#include "stdafx.h"
#include "IRelativeGestureSegment.h"
#include "GestureEnumTypes.h"

#define SwipeLeftLength 3

    /// <summary>
    /// The first part of the swipe left gesture
    /// </summary>
    class SwipeLeftSegment1 : public IRelativeGestureSegment
    {
        /// <summary>
        /// Checks the gesture.
        /// </summary>
        /// <param name="skeleton">The skeleton.</param>
        /// <returns>GesturePartResult based on if the gesture part has been completed</returns>
		public:
			GesturePartResult CheckGesture(const NUI_SKELETON_DATA & skeleton);
    };

    /// <summary>
    /// The second part of the swipe left gesture
    /// </summary>
    class SwipeLeftSegment2 : public IRelativeGestureSegment
    {
        /// <summary>
        /// Checks the gesture.
        /// </summary>
        /// <param name="skeleton">The skeleton.</param>
        /// <returns>GesturePartResult based on if the gesture part has been completed</returns>
		public:
			GesturePartResult CheckGesture(const NUI_SKELETON_DATA & skeleton);
    };

    /// <summary>
    /// The third part of the swipe left gesture
    /// </summary>
    class SwipeLeftSegment3 : public IRelativeGestureSegment
    {
        /// <summary>
        /// Checks the gesture.
        /// </summary>
        /// <param name="skeleton">The skeleton.</param>
        /// <returns>GesturePartResult based on if the gesture part has been completed</returns>
		public:
			GesturePartResult CheckGesture(const NUI_SKELETON_DATA & skeleton);
    };
