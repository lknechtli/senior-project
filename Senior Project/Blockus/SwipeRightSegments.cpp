#include "stdafx.h"
#include "SwipeRightSegments.h"

        GesturePartResult SwipeRightSegment1::CheckGesture(const NUI_SKELETON_DATA & skeleton)
        {

            // //left hand in front of left Shoulder
            if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_LEFT].z < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_LEFT].z && skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].y < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HIP_CENTER].y)
            {
                // Debug.WriteLine("GesturePart 0 - left hand in front of left Shoulder - PASS");
                // //left hand below shoulder height but above hip height
                if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_LEFT].y < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HEAD].y && skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_LEFT].y > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HIP_CENTER].y)
                {
                    // Debug.WriteLine("GesturePart 0 - left hand below shoulder height but above hip height - PASS");
                    // //left hand left of left Shoulder
                    if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_LEFT].x < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_LEFT].x)
                    {
                        // Debug.WriteLine("GesturePart 0 - left hand left of left Shoulder - PASS");
                        return Succeed;
                    }

                    // Debug.WriteLine("GesturePart 0 - left hand left of left Shoulder - UNDETERMINED");
                    return Pausing;
                }

                // Debug.WriteLine("GesturePart 0 - left hand below shoulder height but above hip height - FAIL");
                return Fail;
            }

            // Debug.WriteLine("GesturePart 0 - left hand in front of left Shoulder - FAIL");
            return Fail;
        }
         



		GesturePartResult SwipeRightSegment2::CheckGesture(const NUI_SKELETON_DATA & skeleton)
        {
            // //left hand in front of left Shoulder
            if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_LEFT].z < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_LEFT].z && skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].y < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HIP_CENTER].y)
            {
                // Debug.WriteLine("GesturePart 1 - left hand in front of left Shoulder - PASS");
                // /left hand below shoulder height but above hip height
                if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_LEFT].y < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HEAD].y && skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_LEFT].y > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HIP_CENTER].y)
                {
                    // Debug.WriteLine("GesturePart 1 - left hand below shoulder height but above hip height - PASS");
                    // //left hand left of left Shoulder
                    if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_LEFT].x < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_RIGHT].x && skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_LEFT].x > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_LEFT].x)
                    {
                        // Debug.WriteLine("GesturePart 1 - left hand left of left Shoulder - PASS");
                        return Succeed;
                    }

                    // Debug.WriteLine("GesturePart 1 - left hand left of left Shoulder - UNDETERMINED");
                    return Pausing;
                }

                // Debug.WriteLine("GesturePart 1 - left hand below shoulder height but above hip height - FAIL");
                return Fail;
            }

            // Debug.WriteLine("GesturePart 1 - left hand in front of left Shoulder - FAIL");
            return Fail;
        }



		GesturePartResult SwipeRightSegment3::CheckGesture(const NUI_SKELETON_DATA & skeleton)
        {
            // //left hand in front of left Shoulder
            if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_LEFT].z < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_ELBOW_LEFT].z && skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT].y < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HIP_CENTER].y)
            {
                // //left hand below shoulder height but above hip height
                if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_LEFT].y < skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HEAD].y && skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_LEFT].y > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HIP_CENTER].y)
                {
                    // //left hand left of left Shoulder
                    if (skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_LEFT].x > skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_RIGHT].x)
                    {
                        return Succeed;
                    }

                    return Pausing;
                }

                return Fail;
            }

            return Fail;
        }
