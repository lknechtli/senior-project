#pragma once
#include<vector>
#include<cstdlib>
#include "Piece.h"
#include "Player.h"
class Engine
{
public:
	Engine(void);
	~Engine(void);
	bool loadFiles();
	void cleanUp(void);
	//SDL_Surface *loadImage( std::string filename );
	void applySurface( int x, int y, SDL_Surface* source, SDL_Surface* destination );
	void initBoard(void);
	void displayBoard(int ** board);
	void initBank(void);
	void displayBank(Player * activePlayer);
	void displayPiece(Piece *, int bankPosition);
	SDL_Surface* getSurface(void);
	void setSurface(SDL_Surface * surface);
	void displayActivePiece(Piece *, int x, int y);
	void displaySelectedBankPiece(Player * activePlayer);
	void displayPlayer(Player * activePlayer, int totalScore);
	void displayScore(int score);
private:
	SDL_Surface *tile[5];
	SDL_Surface *background;
	SDL_Surface *screen;
	SDL_Surface *select;
	SDL_Surface *playerSurfaces[2];
	SDL_Surface *scoreSurface;
	SDL_Surface *numbers;
	SDL_Surface * numberSurfs[10];
	static const int infoCoords = 465;
};

