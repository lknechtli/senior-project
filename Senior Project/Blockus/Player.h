#pragma once
#include <vector>
#include<cstdlib>
#include "Piece.h"
using namespace std;
class Player
{
public:
	Player(void);
	~Player(void);
	void init(int ** pieceArray, int pieceColor, int playernum);
	vector<Piece*>::iterator getBank(void);
	vector<Piece*>::iterator getBankEnd(void);
	int getBankIndex(void){return bankSelect;};
	void display(void);
	bool active;
	void passBankInput(signed int key);
	Piece * getActivePiece(void);
	bool removeActivePiece(void);
	bool firstBlock;
	int getPlayerNumber(void){return playerNumber;}
	bool endGame;
		int lastPlacedPiece;
private:
	

	int playerNumber;
	std::vector<Piece*> pieces;
	Piece ** bank;
	void initBank();
	int bankIndex;
	void rotateBankUp(void);
	void rotateBankDown(void);
	void refreshBank(void);
	int totalBank;
	int bankSelect;
};

